<?php include "header.php"; ?>
<div class="container">
    <h2 class="mt-4 mb-4">Lista de Clientes</h2>

    <?php
    include("conexao.php");

    // Verificar se há registros
    $sql = "SELECT * FROM clientes";
    $result = $conn->query($sql);

    if ($result->num_rows > 0) {
        echo "<table class='table'>";
        echo "<thead><tr><th>ID</th><th>Seu Nome</th><th>Sua Data de Nascimento</th><th>CPF</th><th>Seu Email</th><th>Nome do Pet</th><th>Tipo Pet</th><th>Data de Nascimento do Pet</th><th>Sexo do Pet</th><th>Ações</th></tr></thead><tbody>";

        while ($row = $result->fetch_assoc()) {
            echo "<tr>";
            echo "<td>{$row['id']}</td>";
            echo "<td>{$row['nome_cliente']}</td>";
            echo "<td>{$row['sua_data_nascimento']}</td>";
            echo "<td>{$row['cpf']}</td>";
            echo "<td>{$row['email']}</td>";
            echo "<td>{$row['nome_pet']}</td>";
            echo "<td>{$row['tipo_pet']}</td>";
            echo "<td>{$row['data_nascimento_pet']}</td>";
            echo "<td>{$row['sexo_pet']}</td>";
            echo "<td>
                    <a href='editar.php?id={$row['id']}' class='btn btn-info btn-sm mb-1'>Editar</a>
                    <a href='listar_cadastros.php?acao=excluir&id={$row['id']}' class='btn btn-danger btn-sm' onclick=\"return confirm('Tem certeza que deseja excluir este registro?')\">Excluir</a>
                  </td>";
            echo "</tr>";
        }

        echo "</tbody></table>";

        // Processar a exclusão
        if (isset($_GET['acao']) && $_GET['acao'] == 'excluir' && isset($_GET['id'])) {
            $id_excluir = $_GET['id'];

            // Excluir o registro do banco de dados
            $sql_delete = "DELETE FROM clientes WHERE id = $id_excluir";

            if ($conn->query($sql_delete) === TRUE) {
                echo "<div class='alert alert-success mt-3 mb-4'>Registro excluído com sucesso!</div>";
                header("refresh:2;url=listar_cadastros.php");
            } else {
                echo "<div class='alert alert-danger mt-3 mb-4'>Erro ao excluir o registro: " . $conn->error . "</div>";
            }
        }

    } else {
        echo "<p>Nenhum registro encontrado.</p>";
    }

    $conn->close();
    ?>

</div>

<?php include "footer.php"; ?>
