<?php include "header.php"; ?>

<link rel="stylesheet" href="css/login.css">

<div class="container d-flex flex-column justify-content-between" style="min-height: 100vh;">
    <body class="text-center">
        <form method="post" class="form-signin" action="login.php">
            <img class="mt-4 mb-4" src="img/logo.png" alt="" width="72" height="72">
            <h1 class="h3 mb-3 font-weight-normal">Faça login</h1>
            <label for="inputEmail" class="sr-only">Endereço de email</label>
            <input type="email" id="inputEmail" class="form-control" name="email" placeholder="Seu email" required autofocus>
            <label for="inputPassword" class="sr-only">Senha</label>
            <input type="password" id="inputPassword" class="form-control" name="senha" placeholder="Senha" required>
            <div class="checkbox mb-3">
                <label>
                    <input type="checkbox" value="remember-me"> Lembrar de mim
                </label>
            </div>
            <button class="btn btn-lg btn-success btn-block" type="submit">Login</button>
            <p class="mt-3 mb-3 text-muted">&copy; Pet Host 2023</p>
            <p class="mb-2">Ainda não é cadastrado?</p>
            <a class="btn btn-lg btn-info btn-block" href="cadastro.php" role="button">Cadastre-se</a>
        </form>
    </body>

    <?php
        if ($_SERVER["REQUEST_METHOD"] == "POST"){
            $host = "localhost";
            $user = "root";
            $pass = "";
            $db = "pet_db";

            $conn = mysqli_connect($host, $user, $pass, $db);

            if (!$conn) {
                die("Falha na conexão: " . mysqli_connect_error());
            } else {

                $email = $_POST['email'];
                $senha = $_POST['senha'];

                $sql = "SELECT * FROM clientes WHERE email = '$email'";
                $result = mysqli_query($conn, $sql);

                if (mysqli_num_rows($result) > 0) {
                    $row = mysqli_fetch_assoc($result);
                    $id_clienteSQL = $row['id'];
                    $nomeSQL = $row['nome_cliente'];
                    $emailSQL = $row['email'];
                    $senhaSQL = $row['senha'];
                    if (($email == $emailSQL) and ($senha == $senhaSQL)) {
                        session_start();
                        $_SESSION['id_cliente'] = $id_clienteSQL;
                        $_SESSION['nome_cliente'] = $nomeSQL;
                        $_SESSION['usuario'] = $email;                        
                        echo "<div class='alert alert-success mt-3 mb-4'>Login realizado com sucesso.</div>";
                        header("refresh:1.5;url=index.php");
                    } else {
                        echo "<div class='alert alert-danger mt-3 mb-4'>Falha no login.</div>";
                    }
                } else {
                    echo "<div class='alert alert-danger mt-3 mb-4'>Falha no login.</div>";
                }
            }
        }
    ?>
    <?php include "footer.php"; ?>
</div>
