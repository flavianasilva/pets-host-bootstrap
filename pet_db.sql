-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Tempo de geração: 15-Dez-2023 às 10:47
-- Versão do servidor: 10.4.25-MariaDB
-- versão do PHP: 7.4.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Banco de dados: `pet_db`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `clientes`
--

CREATE TABLE `clientes` (
  `id` int(11) NOT NULL,
  `nome_cliente` varchar(100) NOT NULL,
  `sua_data_nascimento` date NOT NULL,
  `cpf` varchar(14) NOT NULL,
  `email` varchar(50) NOT NULL,
  `senha` varchar(14) NOT NULL,
  `nome_pet` varchar(50) NOT NULL,
  `data_nascimento_pet` date NOT NULL,
  `sexo_pet` enum('macho','femea') NOT NULL,
  `tipo_pet` enum('gato','cachorro') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Extraindo dados da tabela `clientes`
--

INSERT INTO `clientes` (`id`, `nome_cliente`, `sua_data_nascimento`, `cpf`, `email`, `senha`, `nome_pet`, `data_nascimento_pet`, `sexo_pet`, `tipo_pet`) VALUES
(5, 'Ana', '1971-01-01', '086333', 'ana@email.com', '123', 'Dina', '1999-01-02', 'femea', 'gato'),
(6, 'Flaviana', '1996-11-01', '12280406489', 'flavianaleandro@outlook.com', '999', 'Garfiu', '2020-04-03', 'macho', 'gato'),
(7, 'Fran', '2000-09-04', '12319434', 'fran@fran.com', '444', 'Maya', '2020-03-03', 'femea', 'gato');

-- --------------------------------------------------------

--
-- Estrutura da tabela `servicos`
--

CREATE TABLE `servicos` (
  `id_servico` int(11) NOT NULL,
  `nome_servico` varchar(255) DEFAULT NULL,
  `tipo_servico` varchar(255) DEFAULT NULL,
  `duracao` int(11) NOT NULL,
  `id_cliente` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Extraindo dados da tabela `servicos`
--

INSERT INTO `servicos` (`id_servico`, `nome_servico`, `tipo_servico`, `duracao`, `id_cliente`) VALUES
(27, 'estadia', 'padrao', 2, 5),
(28, 'banho_tosa', 'premium', 1, 5),
(29, 'estadia', 'padrao', 5, 6),
(30, 'estadia', 'premium', 6, 5),
(31, 'estadia', 'premium', 8, 7),
(32, 'estadia', 'premium', 8, 7),
(33, 'estadia', 'premium', 8, 7),
(34, 'estadia', 'premium', 8, 7),
(35, 'estadia', 'premium', 8, 7),
(36, 'estadia', 'premium', 8, 7),
(37, 'estadia', 'premium', 8, 7),
(38, 'estadia', 'premium', 8, 7),
(39, 'estadia', 'premium', 8, 7),
(40, 'estadia', 'premium', 8, 7),
(41, 'banho_tosa', 'premium', 12, 7);

--
-- Índices para tabelas despejadas
--

--
-- Índices para tabela `clientes`
--
ALTER TABLE `clientes`
  ADD PRIMARY KEY (`id`);

--
-- Índices para tabela `servicos`
--
ALTER TABLE `servicos`
  ADD PRIMARY KEY (`id_servico`),
  ADD KEY `id_cliente` (`id_cliente`);

--
-- AUTO_INCREMENT de tabelas despejadas
--

--
-- AUTO_INCREMENT de tabela `clientes`
--
ALTER TABLE `clientes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT de tabela `servicos`
--
ALTER TABLE `servicos`
  MODIFY `id_servico` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=42;

--
-- Restrições para despejos de tabelas
--

--
-- Limitadores para a tabela `servicos`
--
ALTER TABLE `servicos`
  ADD CONSTRAINT `servicos_ibfk_1` FOREIGN KEY (`id_cliente`) REFERENCES `clientes` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
