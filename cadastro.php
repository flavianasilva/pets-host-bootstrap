<?php include "header.php"; ?>
<div class="container">
    <div class="row">
        <div class="col-md-6">
            <img src="img/cadastro.jpg" alt="Cadastre-se" class="img-fluid img-fluid-custom">
        </div>
        
        <div class="col-md-6">
            <h2 class="mt-2 mb-2">Cadastro do cliente</h2>
            <h2 class="mt-4 mb-4">Cadastro do cliente</h2>
            <form method="post" action="<?php echo $_SERVER['PHP_SELF']; ?>">
                <div class="form-group">
                    <label for="nome_cliente">Seu Nome:</label>
                    <input type="text" class="form-control" name="nome_cliente" required>
                </div>
                <div class="form-group">
                    <label for="sua_data_nascimento">Sua Data de Nascimento:</label>
                    <input type="date" class="form-control" name="sua_data_nascimento" required>
                </div>
                <div class="form-group">
                    <label for="cpf">CPF:</label>
                    <input type="text" class="form-control" name="cpf" required>
                </div>
                <div class="form-group">
                    <label for="email">Seu Email:</label>
                    <input type="email" class="form-control" name="email" required>
                </div>
                <div class="form-group">
                    <label for="password">Senha:</label>
                    <input type="password" class="form-control" name="senha" required>
                </div>
                <div class="form-group">
                    <label for="nome_pet">Nome do Pet:</label>
                    <input type="text" class="form-control" name="nome_pet" required>
                </div>
                <div class="form-group">
                    <label for="tipo_pet">Seu Pet é:</label>
                    <select class="form-control" name="tipo_pet" required>
                        <option value="gato">Gato</option>
                        <option value="cachorro">Cachorro</option>
                    </select>
                </div>
                <div class="form-group">
                    <label for="data_nascimento_pet">Data de Nascimento do Pet:</label>
                    <input type="date" class="form-control" name="data_nascimento_pet" required>
                </div>
                <div class="form-group">
                    <label for="sexo_pet">Sexo do Pet:</label>
                    <select class="form-control" name="sexo_pet" required>
                        <option value="macho">Macho</option>
                        <option value="femea">Fêmea</option>
                    </select>
                </div>
                <button type="submit" class="btn btn-success mb-4">Cadastrar</button>
            </form>

            <?php
            include("conexao.php");

            if ($_SERVER["REQUEST_METHOD"] == "POST") {
                $nome_cliente = $_POST['nome_cliente'];
                $sua_data_nascimento = $_POST['sua_data_nascimento'];
                $cpf = $_POST['cpf'];
                $email = $_POST['email'];
                $senha = $_POST['senha'];
                $nome_pet = $_POST['nome_pet'];
                $tipo_pet = $_POST['tipo_pet'];
                $data_nascimento_pet = $_POST['data_nascimento_pet'];
                $sexo_pet = $_POST['sexo_pet'];

                // Verificação básica dos dados
                if (empty($nome_cliente) || empty($sua_data_nascimento) || empty($cpf) || empty($email) || empty($senha) || empty($nome_pet) || empty($tipo_pet) || empty($data_nascimento_pet) || empty($sexo_pet)) {
                    echo "<div class='alert alert-danger mt-3'>Todos os campos são obrigatórios.</div>";
                } else {
                    // Inserir os dados no banco de dados
                    $sql = "INSERT INTO clientes (nome_cliente, sua_data_nascimento, cpf, email, senha, nome_pet, tipo_pet, data_nascimento_pet, sexo_pet) 
                            VALUES ('$nome_cliente', '$sua_data_nascimento', '$cpf', '$email', '$senha', '$nome_pet', '$tipo_pet', '$data_nascimento_pet', '$sexo_pet')";

                    if ($conn->query($sql) === TRUE) {
                        echo "<div class='alert alert-success mt-3 mb-4'>Sejam bem vindos, $nome_cliente e $nome_pet!</div>";
                    } else {
                        echo "<div class='alert alert-danger mt-3 mb-4'>Erro: " . $sql . "<br>" . $conn->error . "</div>";
                    }
                }
            }

            $conn->close();
            ?>
        </div>
    </div>
</div>

<?php include "footer.php"; ?>
