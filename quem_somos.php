<?php include "header.php"; ?>
<div class="container">
	

    <div class="row">
        <div class="col-md-6">
            <img src="img/quem_somos.jpg" alt="Imagem de Quem Somos" class="img-fluid img-fluid-custom">
        </div>
        
        <div class="col-md-6">
			<p class="text-custom">
			Bem-vindo à nossa creche para gatos, um espaço dedicado ao bem-estar e felicidade dos nossos amigos felinos. Com anos de experiência e amor por esses animais incríveis, oferecemos um ambiente seguro e acolhedor para o seu gato se divertir e socializar.

Na nossa creche, os gatos recebem atenção personalizada, atividades estimulantes e muito carinho. Nossa equipe é composta por amantes de gatos, treinados para garantir que cada gato tenha uma estadia agradável. Acreditamos que cada gato é especial e único, e adaptamos nossas interações de acordo com suas necessidades e personalidade.

Nosso espaço é projetado para proporcionar conforto e entretenimento. Os gatos têm acesso a áreas de brincadeiras, descanso e interação social. Também oferecemos serviços de alimentação de alta qualidade, garantindo que seu gato receba a nutrição adequada durante sua estadia.

Estamos comprometidos com a segurança e felicidade dos gatos que nos são confiados. Ao escolher nossa creche, você está dando ao seu gato a oportunidade de fazer novos amigos, se exercitar e viver experiências enriquecedoras enquanto você está ocupado. Confie em nós para cuidar do seu amado felino como se fosse nosso!.
            </p>
        </div>
    </div>
</div>

<?php include "footer.php"; ?>
