<?php include "header.php"; ?>

<main>
    <div id="myCarousel" class="carousel slide mt-7" data-ride="carousel">
        <ol class="carousel-indicators">
            <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
            <li data-target="#myCarousel" data-slide-to="1"></li>
            <li data-target="#myCarousel" data-slide-to="2"></li>
        </ol>
        <div class="carousel-inner">
            <div class="carousel-item active">
                <img class="first-slide mw-100" src="img/img01a.jpg" alt="First slide">
                <div class="container">
                    <div class="carousel-caption">
                        <h1 class="d-none d-md-block">Creche para Pets</h1>
                        <p class="d-none d-sm-block">Hospedagem para gatos</p>
                        <p><a class="btn btn-lg btn-success text-white" href="servicos.php" role="button">Assine nosso serviço</a></p>
                    </div>
                </div>
            </div>
            <div class="carousel-item">
                <img class="second-slide mw-100" src="img/img02a.jpg" alt="Second slide">
                <div class="container">
                    <div class="carousel-caption">
                        <h1 class="d-none d-md-block">Você sabia?</h1>
                        <p class="d-none d-sm-block">Os gatos costumam dormir 16h por dia</p>
                        <p><a class="btn btn-lg btn-success text-white" href="#" role="button">Saiba mais</a></p>
                    </div>
                </div>
            </div>
            <div class="carousel-item">
                <img class="third-slide mw-100" src="img/img03a.jpg" alt="Third slide">
                <div class="container">
                    <div class="carousel-caption">
                        <h1 class="d-none d-md-block">Agende uma visita</h1>
                        <p class="d-none d-sm-block">Melhores confortos para seu Pet</p>
                        <p><a class="btn btn-lg btn-success text-white" href="#" role="button">Veja nossa galeria</a></p>
                    </div>
                </div>
            </div>
        </div>
        <a class="carousel-control-prev" href="#myCarousel" role="button" data-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span class="sr-only">Voltar</span>
        </a>
        <a class="carousel-control-next" href="#myCarousel" role="button" data-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
            <span class="sr-only">Avançar</span>
        </a>
    </div>

    <div class="container text-center">
        <div class="row">
            <!-- Atividades Modal -->
            <div class="col-lg-3 col-md-6 col-12">
                <img class="my-3 rounded-circle" src="img/img03a.jpg" alt="Generic placeholder image" width="140" height="140">
                <h2>Atividades</h2>
                <p class="text-justify">Na Pets Host seu gato passa o dia brincando...</p>
                <p><a class="btn btn-secondary" href="#" role="button" data-toggle="modal" data-target="#modalAtividades">Veja mais detalhes &raquo;</a></p>
            </div>

            <!-- Conforto Modal -->
            <div class="col-lg-3 col-md-6 col-12">
                <img class="my-3 rounded-circle" src="img/img02a.jpg" alt="Generic placeholder image" width="140" height="140">
                <h2>Conforto</h2>
                <p class="text-justify">Temos a melhor estrutura para seu gato...</p>
                <p><a class="btn btn-secondary" href="#" role="button" data-toggle="modal" data-target="#modalConforto">Veja mais detalhes &raquo;</a></p>
            </div>

            <!-- Alimentação Modal -->
            <div class="col-lg-3 col-md-6 col-12">
                <img class="my-3 rounded-circle" src="img/img03a.jpg" alt="Generic placeholder image" width="140" height="140">
                <h2>Alimentação</h2>
                <p class="text-justify">Temos parceria com os melhores fornecedores...</p>
                <p><a class="btn btn-secondary" href="#" role="button" data-toggle="modal" data-target="#modalAlimentacao">Veja mais detalhes &raquo;</a></p>
            </div>

            <!-- Uma boa opção Modal -->
            <div class="col-lg-3 col-md-6 col-12">
                <img class="my-3 rounded-circle" src="img/img01a.jpg" alt="Generic placeholder image" width="140" height="140">
                <h2>Uma boa opção?</h2>
                <p class="text-justify">Deixar meu gato numa creche é uma boa opção?...</p>
                <p><a class="btn btn-secondary" href="#" role="button" data-toggle="modal" data-target="#modalBoaOpcao">Veja mais detalhes &raquo;</a></p>
            </div>
        </div>
    </div>
</main>

<!-- Modais -->

<!-- Modal para "Atividades" -->
<div class="modal fade" id="modalAtividades" tabindex="-1" role="dialog" aria-labelledby="modalAtividadesLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="modalAtividadesLabel">Detalhes das Atividades</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Fechar">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p>Na Pets Host seu gato passa o dia brincando, fazendo atividades físicas, mentais e novas amizades.</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Voltar</button>
            </div>
        </div>
    </div>
</div>

<!-- Modal para "Conforto" -->
<div class="modal fade" id="modalConforto" tabindex="-1" role="dialog" aria-labelledby="modalConfortoLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="modalAtividadesLabel">Detalhes dos confortos</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Fechar">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p>Temos a melhor estrutura para seu gato. A estrutura comporta cerca de 50 gatos com amplo espaço para eles se divertirem.</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Voltar</button>
            </div>
        </div>
    </div>
</div>

<!-- Modal para "Alimentação" -->
<div class="modal fade" id="modalAlimentacao" tabindex="-1" role="dialog" aria-labelledby="modalAlimentacaoLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="modalAlimentacaoLabel">Detalhes da alimentação</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Fechar">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p>Temos parceria com os melhores fornecedores de ração para gatos. Seu gato terá a melhor alimentação, sendo acompanhado por nutricionistas especializados.</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Voltar</button>
            </div>
        </div>
    </div>
</div>

<!-- Modal para "Uma boa opção?" -->
<div class="modal fade" id="modalBoaOpcao" tabindex="-1" role="dialog" aria-labelledby="modalBoaOpcaoLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="modalBoaOpcaoLabel">Detalhes das boas opções</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Fechar">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p>Deixar meu gato numa creche é uma boa opção? Sabemos que a maioria das pessoas precisam trabalhar o dia todo, porém, também sabemos que os animais necessitam de companhia, passeios, exercícios e muito afeto.</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Voltar</button>
            </div>
        </div>
    </div>
</div>

<?php include "footer.php"; ?>
