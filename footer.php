<footer class="bg-success text-white">
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <h4>Contato</h4>
                <p>Telefone: (123) 456-7890</p>
                <p>Email: info@petshost.com</p>
                <p>Endereço: Rua Gatolandia, 1234 - Mialcife - Pernangato</p>
            </div>
            <div class="col-md-6">
                <h4>Redes Sociais</h4>
                <a href="#" class="text-dark"><i class="fab fa-facebook-square"></i> Facebook</a><br>
                <a href="#" class="text-dark"><i class="fab fa-twitter-square"></i> Twitter</a><br>
                <a href="#" class="text-dark"><i class="fab fa-instagram"></i> Instagram</a>
            </div>
        </div>
    </div>
</footer>
