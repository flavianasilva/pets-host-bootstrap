<?php
include "header.php";
include "conexao.php";

// Verificar se o ID foi fornecido
if (!isset($_GET['id'])) {
    echo "<div class='container mt-4'><p>ID não fornecido.</p></div>";
    include "footer.php";
    exit;
}

$id = $_GET['id'];

// Buscar os dados do registro pelo ID
$sql = "SELECT * FROM clientes WHERE id = $id";
$result = $conn->query($sql);

if ($result->num_rows == 0) {
    echo "<div class='container mt-4'><p>Registro não encontrado.</p></div>";
    include "footer.php";
    exit;
}

$row = $result->fetch_assoc();

// Processar o formulário se for enviado
if ($_SERVER["REQUEST_METHOD"] == "POST") {
    $nome_cliente = $_POST['nome_cliente'];
    $sua_data_nascimento = $_POST['sua_data_nascimento'];
    $cpf = $_POST['cpf'];
    $senha = $_POST['senha'];
    $nome_pet = $_POST['nome_pet'];
    $tipo_pet = $_POST['tipo_pet'];
    $data_nascimento_pet = $_POST['data_nascimento_pet'];
    $sexo_pet = $_POST['sexo_pet'];

    // Atualizar os dados no banco de dados
    $sql_update = "UPDATE clientes SET 
                    nome_cliente = '$nome_cliente', 
                    sua_data_nascimento = '$sua_data_nascimento', 
                    cpf = '$cpf', 
                    email = '$email', 
                    nome_pet = '$nome_pet', 
                    tipo_pet = '$tipo_pet', 
                    data_nascimento_pet = '$data_nascimento_pet', 
                    sexo_pet = '$sexo_pet'
                    WHERE id = $id";

    if ($conn->query($sql_update) === TRUE) {
        echo "<div class='container mt-4'>
                <div class='mb-4'>.</div>
                <div class='alert alert-success mb-4'>Registro atualizado com sucesso!</div>
            </div>";
    } else {
        echo "<div class='container mt-4'>
                <div class='alert alert-danger mb-4'>Erro ao atualizar o registro: " . $conn->error . "</div>
            </div>";
    }
}

$conn->close();
?>

<div class="container mt-4">
    <h2>Editar Cliente</h2>
    <form method="post" action="<?php echo $_SERVER['PHP_SELF'] . '?id=' . $id; ?>">
        <!-- Campos do formulário preenchidos com os dados do registro -->
        <div class="form-group">
            <label for="nome_cliente">Seu Nome:</label>
            <input type="text" class="form-control" name="nome_cliente" value="<?php echo $row['nome_cliente']; ?>" required>
        </div>
        <div class="form-group">
            <label for="sua_data_nascimento">Sua Data de Nascimento:</label>
            <input type="date" class="form-control" name="sua_data_nascimento" value="<?php echo $row['sua_data_nascimento']; ?>" required>
        </div>
        <div class="form-group">
            <label for="cpf">CPF:</label>
            <input type="text" class="form-control" name="cpf" value="<?php echo $row['cpf']; ?>" required>
        </div>
        <div class="form-group">
            <label for="email">Seu Email:</label>
            <input type="email" class="form-control" name="email" value="<?php echo $row['email']; ?>" required>
        </div>
        <div class="form-group">
            <label for="nome_pet">Nome do Pet:</label>
            <input type="text" class="form-control" name="nome_pet" value="<?php echo $row['nome_pet']; ?>" required>
        </div>
        <div class="form-group">
            <label for="tipo_pet">Seu Pet é:</label>
            <select class="form-control" name="tipo_pet" required>
                <option value="gato">Gato</option>
                <option value="cachorro">Cachorro</option>
            </select>
        </div>
        <div class="form-group">
            <label for="data_nascimento_pet">Data de Nascimento do Pet:</label>
            <input type="date" class="form-control" name="data_nascimento_pet" value="<?php echo $row['data_nascimento_pet']; ?>" required>
        </div>
        <div class="form-group">
            <label for="sexo_pet">Sexo do Pet:</label>
            <select class="form-control" name="sexo_pet" value="<?php echo $row['sexo_pet']; ?>" required>
                <option value="macho">Macho</option>
                <option value="femea">Fêmea</option>
            </select>
        </div>

        <button type="submit" class="btn btn-success mb-4">Atualizar</button>
    </form>
</div>

<?php include "footer.php"; ?>
