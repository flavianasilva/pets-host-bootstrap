<?php
include "header.php";

// Verificar se o usuário está logado
session_start();
if (!isset($_SESSION['usuario'])) {
    // Se não estiver logado, redirecionar para a página de login
    header("Location: login.php");
    exit(); // Certifique-se de parar a execução aqui
}

// Dados do cliente logado
$clienteLogado = $_SESSION['nome_cliente'];
$id_cliente = $_SESSION['id_cliente'];

?>

<div class="container">
    <div class="row">
        <div class="col-md-6">
            <img src="img/servicos.jpg" alt="Serviços" class="img-fluid img-fluid-custom">
        </div>
        
        <div class="col-md-6">
            <!-- <h2 class="mt-4 mb-4">Orçamento de serviços</h2> -->
            <h2 class="mt-4 mb-4">Olá <?php echo $clienteLogado; ?>, faça seu orçamento:</h2>
            <form method="post" action="<?php echo $_SERVER['PHP_SELF']; ?>">
                <div class="form-group">
                    
                </div>
                <div class="form-group">
                    <label for="nome_servico">Serviço:</label>
                    <select class="form-control" name="nome_servico" required>
                        <option value="estadia">Estadia</option>
                        <option value="banho_tosa">Banho e tosa</option>
                    </select>
                </div>
                <div class="form-group">
                    <label for="tipo_servico">Tipo de serviço:</label>
                    <select class="form-control" name="tipo_servico" required>
                        <option value="padrao">Padrão</option>
                        <option value="premium">Premium</option>
                    </select>
                </div>
                <div class="form-group">
                    <label for="duracao">Duração (Se estadia):</label>
                    <input type="text" class="form-control" name="duracao" required>
                </div>
                <button type="submit" class="btn btn-success mb-4">Solicitar</button>
            </form>

            <?php
            include("conexao.php");

            if ($_SERVER["REQUEST_METHOD"] == "POST") {
                $nome_servico = $_POST['nome_servico'];
                $tipo_servico = $_POST['tipo_servico'];
                $duracao = $_POST['duracao'];

                // Verificação básica dos dados
                if (empty($nome_servico) || empty($tipo_servico) || empty($duracao)) {
                    echo "<div class='alert alert-danger mt-3'>Todos os campos são obrigatórios.</div>";
                } else {
                    // Inserir os dados no banco de dados
                    $sql = "INSERT INTO servicos (nome_servico, tipo_servico, duracao, id_cliente)
                            VALUES ('$nome_servico', '$tipo_servico', '$duracao', '$id_cliente')";

                    if ($conn->query($sql) === TRUE) {
                        echo "<div class='alert alert-success mt-3 mb-4'>Serviço solicitado.</div>";
                    } else {
                        echo "<div class='alert alert-danger mt-3 mb-4'>Erro: " . $sql . "<br>" . $conn->error . "</div>";
                    }
                }
            }

            $conn->close();
            ?>
        </div>
    </div>
</div>

<?php include "footer.php"; ?>
